#pragma once

#include <iostream>

class TicTacToe
{
private:

	char m_board[9] = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
	int m_numTurns = 0;
	char m_playerTurn = 'X';
	char m_winner;

	bool CheckWinConditionForPlayer(char player)
	{
		if ((m_board[0] == player && m_board[1] == player && m_board[2] == player) ||
			(m_board[3] == player && m_board[4] == player && m_board[5] == player) ||
			(m_board[6] == player && m_board[7] == player && m_board[8] == player) ||

			(m_board[0] == player && m_board[3] == player && m_board[6] == player) ||
			(m_board[1] == player && m_board[4] == player && m_board[7] == player) ||
			(m_board[2] == player && m_board[5] == player && m_board[8] == player) ||

			(m_board[0] == player && m_board[4] == player && m_board[8] == player) ||
			(m_board[2] == player && m_board[4] == player && m_board[6] == player))
		{
			m_winner = player;
			return true;
		}

		return false;
	}

public:

	TicTacToe() { }

	void DisplayBoard() const
	{
		std::cout << "[" << m_board[0] << "][" << m_board[1] << "][" << m_board[2] << "]" << std::endl;
		std::cout << "[" << m_board[3] << "][" << m_board[4] << "][" << m_board[5] << "]" << std::endl;
		std::cout << "[" << m_board[6] << "][" << m_board[7] << "][" << m_board[8] << "]" << std::endl;
	}

	bool IsOver()
	{
		bool over = false;

		if (CheckWinConditionForPlayer('X') ||
			CheckWinConditionForPlayer('O')) return true;

		return over;
	}

	char GetPlayerTurn() const
	{
		return m_playerTurn;
	}

	bool IsValidMove(int pos) const
	{
		if ((pos > 0 && pos < 10) && m_board[pos - 1] == ' ') return true;
		else return false;
	}

	void Move(int pos)
	{
		m_board[pos - 1] = m_playerTurn;
		if (m_playerTurn == 'X') m_playerTurn = 'O';
		else m_playerTurn = 'X';
	}

	void DisplayResult() const
	{
		std::cout << "The Winner is: " << m_winner << "!" << std::endl;
	}
};